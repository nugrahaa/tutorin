from django.urls import path
from . import views

urlpatterns = [
    path('', views.getvideos, name="offline-videos"),
]
